import React from "react";
import Todo from "./Todo";

function TodoList({ todos, toggleComplete, removeTodo, editTodo }) {
  return (
    <ul style = {{ listStyle:"none"}}>
      {todos.map((todo) => (
        <Todo
          key={todo.id}
          todo={todo}
          toggleComplete={toggleComplete}
          removeTodo={removeTodo}
          editTodo={editTodo}
        />
      ))}
    </ul>
  );
}

export default TodoList;
