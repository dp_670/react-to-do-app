import React from "react";
import '../css/Todo.css';

function Todo({ todo, toggleComplete, removeTodo,editTodo }) {
  function handleCheckBoxClick() {
    toggleComplete(todo.id);
  }
  function handleRemoveClick() {
    removeTodo(todo.id);
    }
    function handleEditClick() {
        editTodo(todo.id,prompt("edit your task",todo.task));
    }
  return (
    <div className="task" >
      <input type="checkbox" onClick={handleCheckBoxClick} />
      <li
        style={{
          color: "white",
          textDecoration: todo.completed ? "line-through" : null,
          backgroundColor:"grey",
          fontSize: 15,
          padding: 5,
          borderRadius:10,
        }}
      >
        {todo.task}
      </li>
      <span>
      <button className="btn" id="b1" onClick={handleRemoveClick}>Delete</button>
      <button className="btn" id="b2" onClick={handleEditClick}>Edit</button>
      </span>
      
      </div>
  );
}
export default Todo;
