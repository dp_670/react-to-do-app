import React, { useState } from "react";
import uuid from "uuid";
import '../css/Todo.css';
function TodoForm({ addTodo }) {
  const [todo, setTodo] = useState({
    id: "",
    task: "",
    completed: false,
  });
  function handleTaskInputChange(e) {
    setTodo({ ...todo, task: e.target.value });
  }
  function handleSubmit(e) {
    e.preventDefault();
    if (todo.task.trim()) {
        addTodo({ ...todo, id: uuid.v4() });
        setTodo({ ...todo, task: "" });
    }
  }
  return (
    <form className="form" onSubmit={handleSubmit}>
      <input
        className="input"
        name="task"
        type="text"
        value={todo.task}
        onChange={handleTaskInputChange}
        placeholder="Enter task here"
      />
      <button className="btn3" type="submit">submit</button>
    </form>
  );
}

export default TodoForm;
