import React,{useEffect,useState} from 'react';
import './App.css';
import TodoForm from "./components/TodoForm";
import TodoList from "./components/TodoList";

function App() {
  const LOCAL_STORAGE_KEY = "react-todo-list-todos";
  const [todos, setTodos] = useState([]);
 
  useEffect(() => {
    const storageTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    if (storageTodos) {
      setTodos(storageTodos);
  }
  }, []);
  
  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos));
  }, [todos]);
  
  function addTodo(todo) {
    setTodos([todo, ...todos]);
  }

  function toggleComplete(id) {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo, completed: !todo.completed
          };
        }
        return todo;
      })
    );
  }
  function removeTodo(id) {
    setTodos(todos.filter(todo => todo.id !== id));
  }
  function editTodo(id,task) {
    setTodos(
      todos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo, task:task ,id:id
          };
        }
        return todo;
      })
    );
  }
  return (
    <div className="App">
      <header className="App-header">
        <div className="main_div">
        <p>React to-do App</p>
        <TodoForm addTodo={addTodo} />
        <TodoList todos={todos}
          toggleComplete={toggleComplete}
          removeTodo={removeTodo}
          editTodo={editTodo}
        />
        </div>
      </header>
    </div>
  );
}

export default App;
